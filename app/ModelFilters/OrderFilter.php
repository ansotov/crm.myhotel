<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

/**
 * Class OrderFilter
 *
 * @package App\ModelFilters\Orders
 */
class OrderFilter extends ModelFilter
{
	/**
	 * @var array
	 */
	public $relations = [];

	/**
	 * @param int $id
	 *
	 * @return \App\ModelFilters\OrderFilter
	 */
	public function status(int $id): OrderFilter
	{
		return $this->where('orders.status_id', $id);
	}

	/**
	 * @param string $dates
	 *
	 * @return \App\ModelFilters\OrderFilter
	 */
	public function dateRange(string $dates): OrderFilter
	{
		$dates = explode(' - ', $dates);

		$dateFrom = date('Y-m-d H:i:s', ($dates[0] ? strtotime($dates[0]) : strtotime(now())));
		$dateTo   = date('Y-m-d 23:59:59', ($dates[1] ? strtotime($dates[1]) : strtotime(now())));

		return $this->whereBetween(
			'orders.created_at',
			[
				$dateFrom,
				$dateTo
			]
		);
	}

	/**
	 * @param string $str
	 *
	 * @return \App\ModelFilters\OrderFilter
	 */
	public function search(string $str): OrderFilter
	{
		return $this->leftJoin('rooms', 'orders.room_id', 'rooms.id')
			->where(function ($q) use ($str) {
				return $q->where('orders.slug', 'LIKE', $str . '%')
					->orWhere('rooms.number', 'LIKE', $str . '%')
					->orWhere('orders.comment', 'LIKE', '%' . $str . '%');
			});
	}
}
