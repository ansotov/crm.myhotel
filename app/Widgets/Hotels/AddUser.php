<?php

namespace App\Widgets\Hotels;

use Arrilot\Widgets\AbstractWidget;

/**
 * Class AddUser
 *
 * @package App\Widgets\Hotels
 */
class AddUser extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.hotels.add_user', [
            'config' => $this->config,
        ]);
    }
}
