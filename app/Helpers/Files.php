<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 2020-05-13
 * Time: 17:55
 */


use Illuminate\Support\Facades\File;

if (! function_exists('customFolder')) {
    /**
     * Custom path
     *
     * @param $path
     *
     * @return mixed
     */
    function customFolder($path)
    {
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

        return $path;
    }
}

if (! function_exists('hotelFolder')) {
    /**
     * Hotel folder
     *
     * @param      $folder
     * @param bool $type
     *
     * @return mixed
     */
    function hotelFolder($folder, $type = false)
    {
        if ($type) {
            return customFolder(storage_path('/app/public/hotels/' . $folder . DIRECTORY_SEPARATOR . $type));
        } else {
            return customFolder(storage_path('/app/public/hotels/' . $folder));
        }
    }
}

if (! function_exists('tempFolderPath')) {
    /**
     * Temp folder path
     *
     * @param $folder
     *
     * @return string
     */
    function tempFolderPath($folder): string
    {
        return asset(DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . 'temp');
    }
}

if (! function_exists('frontImages')) {
    /**
     * @param $image
     *
     * @return string
     */
    function frontImages($image): string
    {
        return config('app.static_url') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $image;
    }
}
