<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 2020-06-14
 * Time: 15:45
 */

use Stichoza\GoogleTranslate\GoogleTranslate;

if (! function_exists('googleTrans')) {
    /**
     * @param $text
     * @param $to
     * @param $from
     *
     * @return string|null
     * @throws \ErrorException
     */
    function googleTrans($text, $to, $from): ?string
    {
        sleep(1);
        return GoogleTranslate::trans(html_entity_decode(strip_tags($text)), $to, $from);
    }
}

if (! function_exists('ucfirst_utf8')) {
    /**
     * @param $str
     *
     * @return string
     */
    function ucfirst_utf8($str): string
    {
        return mb_substr(mb_strtoupper($str, 'utf-8'), 0, 1, 'utf-8') .
            mb_substr(mb_strtolower($str, 'utf-8'), 1, mb_strlen($str)-1, 'utf-8');
    }
}
