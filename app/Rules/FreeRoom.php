<?php

namespace App\Rules;

use App\Models\Hotels\HotelGuest;
use Illuminate\Contracts\Validation\Rule;

class FreeRoom implements Rule
{
    /**
     * @var object
     */
    private $item;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $this->item = HotelGuest::where('start_at', '>=', $value)
            ->whereBetween('end_at', [])
            ->first();

        return ! $this->item;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return ':attribute ' . $this->item->start_at . ' - ' . $this->item->end_at;
    }
}
