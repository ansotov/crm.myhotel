<?php

namespace App\Jobs\Hotels;

use App\Models\Offers\Offer;
use App\Models\Offers\OfferData;
use App\Traits\Languages;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class OfferCreate
 *
 * @package App\Jobs\Hotels
 */
class OfferCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var int
     */
    private $hotelId;

    /**
     * OfferCreate constructor.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $hotelId
     */
    public function __construct(Request $request, $hotelId)
    {
        $this->request = $request;
        $this->hotelId = $hotelId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $item = new Offer(
            [
                'hotel_id' => $this->hotelId,
                'slug'     => $this->request['slug'],
                'active'   => (isset($this->request['active']) ? 1 : 0),
            ]
        );
        $item->save();

        try {
            foreach ($this->hotelLanguages($this->hotelId) as $v) {
                $subItem = new OfferData(
                    [
                        'lang_id'     => $v->lang_id,
                        'offer_id'    => $item->id,
                        'title'       => $this->request['title'],
                        'description' => $this->request['description'],
                        'text'        => $this->request['text'],
                    ]
                );
                $subItem->save();
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
