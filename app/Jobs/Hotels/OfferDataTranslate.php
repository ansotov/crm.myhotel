<?php

namespace App\Jobs\Hotels;

use App\Models\Offers\Offer;
use App\Models\Offers\OfferData;
use App\Traits\Languages;
use App\Traits\Logs\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class OfferDataTranslate
 *
 * @package App\Jobs\Hotels
 */
class OfferDataTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages, Data;

    /**
     * @var int
     */
    public $tries = 5;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var \App\Models\Offers\Offer
     */
    private $item;

    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * @var string
     */
    private $fromLang;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Offer $item, $request, $fromLang, $user)
    {
        $this->request  = $request;
        $this->item     = $item;
        $this->user     = $user;
        $this->fromLang = $fromLang;
    }

    /**
     * @throws \ErrorException
     */
    public function handle()
    {
        foreach ($this->hotelLanguages($this->item->hotel_id) as $v) {
            OfferData::updateOrCreate(
                [
                    'offer_id' => $this->item->id,
                    'lang_id'  => $v->lang_id,
                ],
                [
                    'title'       => isset($this->request['title'][$v->lang_id])
                        ? $this->request['title'][$v->lang_id]
                        : ucfirst(googleTrans(
                            $this->request['title'][$this->fromLang->id],
                            $v->language->const,
                            $this->fromLang->const
                        )),
                    'description' => ! is_null($this->request['description'][$v->lang_id])
                        ? $this->request['description'][$v->lang_id]
                        : (! is_null($this->request['description'][$this->fromLang->id])
                            ? ucfirst(googleTrans(
                                $this->request['description'][$this->fromLang->id],
                                $v->language->const,
                                $this->fromLang->const
                            )) : ''),
                    'text'        => ! is_null($this->request['text'][$v->lang_id])
                        ? $this->request['text'][$v->lang_id]
                        : (! is_null($this->request['text'][$this->fromLang->id])
                            ? ucfirst(googleTrans(
                                $this->request['text'][$this->fromLang->id],
                                $v->language->const,
                                $this->fromLang->const
                            )) : ''),
                ]
            );
        }

        // Log data
        Log::info('Queue finished ', $this->dataArray($this->item, __CLASS__));
    }
}
