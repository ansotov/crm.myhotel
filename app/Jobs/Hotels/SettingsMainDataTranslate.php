<?php

namespace App\Jobs\Hotels;

use App\Models\Hotels\Hotel;
use App\Models\Hotels\HotelData;
use App\Traits\Languages;
use App\Traits\Logs\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class SettingsMainDataTranslate
 *
 * @package App\Jobs\Hotels
 */
class SettingsMainDataTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages, Data;

    /**
     * @var int
     */
    public $tries = 5;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var \App\Models\Hotels\Hotel
     */
    private $item;

    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * @var string
     */
    private $fromLang;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Hotel $item, $request, $fromLang, $user)
    {
        $this->request  = $request;
        $this->item     = $item;
        $this->user     = $user;
        $this->fromLang = $fromLang;
    }

    /**
     * @throws \ErrorException
     */
    public function handle()
    {
        foreach ($this->hotelLanguages($this->item->id) as $v) {
            HotelData::updateOrCreate(
                [
                    'hotel_id' => $this->item->id,
                    'lang_id'  => $v->lang_id,
                ],
                [
                    'description' => ! is_null($this->request[$v->lang_id]['description'])
                        ? $this->request[$v->lang_id]['description']
                        : (! is_null($this->request[$this->fromLang->id]['description'])
                            ? ucfirst(googleTrans(
                                $this->request[$this->fromLang->id]['description'],
                                $v->language->const,
                                $this->fromLang->const
                            )) : ''),
                ]
            );
        }

        // Log data
        Log::info('Queue finished ', $this->dataArray($this->item, __CLASS__));
    }
}
