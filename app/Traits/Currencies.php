<?php

namespace App\Traits;

use App\Models\Currency;
use App\Models\Hotels\HotelCurrency;

/**
 * Trait Currencies
 *
 * @package App\Traits
 */
trait Currencies
{
    /**
     * @return \App\Models\Currency[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function currencies()
    {
        return Currency::all();
    }

    /**
     * @param bool $hotelId
     *
     * @return mixed
     */
    protected function hotelCurrencies(bool $hotelId)
    {
        return HotelCurrency::whereHotelId($hotelId)->get();
    }
}
