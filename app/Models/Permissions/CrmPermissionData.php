<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmPermissionData extends Model
{
	use HasFactory;

	/**
	 * @var bool
	 */
	public $timestamps = false;
}
