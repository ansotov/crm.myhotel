<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static main($main)
 * @method static where(array $array)
 */
class CrmRole extends Model
{
	use HasFactory;

	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param bool                                  $main
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeMain(Builder $query, bool $main = true): Builder
	{
		return $query->where('main', $main);
	}
}
