<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class CrmPermissionRole extends Model
{
	use HasFactory;

	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @var string[]
	 */
	protected $fillable = [
		'hotel_id',
		'permission_id',
		'role_id',
	];
}
