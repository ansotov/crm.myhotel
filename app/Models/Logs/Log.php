<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 *
 * @package App\Models\Logs
 */
class Log extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql_logs';
}
