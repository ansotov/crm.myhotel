<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 *
 * @package App\Models\Logs
 */
class Event extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql_logs';

    /**
     * @var string[]
     */
    protected $fillable = [
        'type_id',
        'user_id',
        'object_id',
    ];
}
