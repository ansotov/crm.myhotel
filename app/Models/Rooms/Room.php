<?php

namespace App\Models\Rooms;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * Class Room
 *
 * @package App\Models\Rooms
 * @method static hotelRooms()
 * @method static find(int $id)
 * @method static where(int[] $array)
 */
class Room extends Model
{
    use SoftDeletes, Notifiable;

    /**
     * @var string[]
     */
    protected $dispatchesEvents = [
        'deleted' => \App\Events\Hotels\Room::class,
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'type_id',
        'number',
        'comment',
        'deleted',
        'active',
    ];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo('App\Models\Rooms\RoomType', 'type_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelRooms(Builder $query): Builder
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAsc(Builder $query, $field = 'number'): Builder
    {
        return $query->orderByRaw('LENGTH(' . $field . ') ASC')->orderBy($field);
    }
}
