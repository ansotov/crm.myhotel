<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class HotelLanguage
 *
 * @package App\Models\Hotels
 * @method static create(array $array)
 * @method static where(array $array)
 */
class HotelLanguage extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'lang_id',
        'order',
    ];

    /**
     * Hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function hotel(): BelongsTo
    {
        return $this->belongsTo('App\Models\Hotels\Hotel', 'hotel_id', 'id');
    }

    /**
     * Language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function language(): BelongsTo
    {
        return $this->belongsTo('App\Models\Language', 'lang_id', 'id');
    }
}
