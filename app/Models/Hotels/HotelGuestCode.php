<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HotelGuestCode
 *
 * @package App\Models\Hotels
 * @method static where(array $array)
 */
class HotelGuestCode extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'guest_id',
        'code',
    ];
}
