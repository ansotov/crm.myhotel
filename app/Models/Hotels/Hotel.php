<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @method static create()
 * @method static whereId($hotel_id)
 * @method static find($hotel_id)
 */
class Hotel extends Model
{
    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo('App\Models\Hotels\HotelData', 'id', 'hotel_id')
            ->where(['lang_id' => $langId]);
    }

    /**
     * Guests
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guests(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'id', 'hotel_id');
    }

    /**
     * Languages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Language',
            config('database.connections.mysql.database') . '.hotel_languages',
            'hotel_id',
            'lang_id'
        )
            ->orderBy('order');
    }

    /**
     * Currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Currency',
            config('database.connections.mysql.database') . '.hotel_currencies',
            'hotel_id'
        )
            ->orderBy('order');
    }
}
