<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HotelUser
 *
 * @package App\Models\Hotels
 * @method static where(array $array)
 * @method static firstOrCreate(array $array, array $array1)
 * @method static select(string $string, string $string1, string $string2, string $string3)
 */
class HotelUser extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'hotel_id',
        'name',
        'phone',
        'email',
        'personal_data',
    ];
}
