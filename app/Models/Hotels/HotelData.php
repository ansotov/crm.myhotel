<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HotelData
 *
 * @package App\Models\Hotels
 * @method static create(array $array)
 * @method static where(array $array)
 */
class HotelData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'hotel_id',
        'title',
        'description',
    ];
}
