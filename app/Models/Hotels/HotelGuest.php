<?php

namespace App\Models\Hotels;

use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorAlias;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static hotelGuests()
 */

/**
 * Class HotelGuest
 *
 * @package App\Models\Hotels
 * @method static hotelGuests()
 * @method static where(array $array)
 * @method static find(int $id)
 */
class HotelGuest extends Model
{
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $dates = [
        'start_at',
        'end_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'room_id',
        'user_id',
        'start_at',
        'end_at',
        'comment',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo('App\Models\Rooms\Room', 'room_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\Hotels\HotelUser', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function code(): BelongsTo
    {
        return $this->belongsTo('App\Models\Hotels\HotelGuestCode', 'id', 'guest_id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return LengthAwarePaginatorAlias
     */
    public function scopeHotelGuests(Builder $query): LengthAwarePaginatorAlias
    {
        return $query->leftJoin('rooms', 'hotel_guests.room_id', '=', 'rooms.id')
            ->where('rooms.hotel_id', auth()->user()->hotel_id)
            ->select('hotel_guests.*')
            ->paginate(setting('site.perpage'));
    }
}
