<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewData
 *
 * @package App\Models\News
 * @method static where(int[] $array)
 */
class NewData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'new_id',
        'title',
        'description',
        'text',
    ];
}
