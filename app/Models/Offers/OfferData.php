<?php

namespace App\Models\Offers;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OfferData
 *
 * @package App\Models\Offers
 * @method static updateOrCreate(array $array, array $array1)
 * @method static where(array $array)
 */
class OfferData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'offer_id',
        'title',
        'description',
        'text',
    ];
}
