<?php

namespace App\Models\Offers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Offer
 *
 * @package App\Models\Offers
 * @method static archive()
 * @method static hotelOffers()
 * @method static find(int $id)
 * @method static where(int[] $array)
 */
class Offer extends Model
{
    /**
     * @var string[]
     */
    protected $dates = [
        'publish_at',
        'deleted_at',
        'start_at',
        'end_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'start_at',
        'end_at',
        'active',
    ];

    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\Offers\OfferData',
            'id',
            'offer_id'
        )
            ->where(['lang_id' => $langId]);
    }

    /**
     * Images
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany('App\Models\Offers\OfferImage', 'offer_id', 'id');
    }

    /**
     * Scope a query to only archive.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeArchive(Builder $query): Builder
    {
        return $query->where(['archive' => true, 'hotel_id' => auth()->user()->hotel_id]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelOffers(Builder $query): Builder
    {
        return $query->where(['archive' => false, 'hotel_id' => auth()->user()->hotel_id]);
    }
}
