<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LanguageData
 *
 * @package App\Models
 */
class LanguageData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $connection = 'mysql_geo';

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'language_id',
        'title',
    ];
}
