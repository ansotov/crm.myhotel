<?php

namespace App\Models\Articles;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static where(Closure $param)
 * @method static hotelItem()
 */

/**
 * Class ContactArticle
 *
 * @package App\Models\Articles
 * @method static create(array $array)
 * @method static where(Closure $param)
 * @method static hotelItem()
 */
class ContactArticle extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\BelongsTo|object|null
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\Articles\ContactArticleData',
            'id',
            'article_id'
        )
            ->where(['lang_id' => $langId]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelItem(Builder $query): Builder
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }
}
