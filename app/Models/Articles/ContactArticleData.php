<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(array $array)
 */

/**
 * Class ContactArticleData
 *
 * @package App\Models\Articles
 * @method static create(array $array)
 * @method static where(array $array)
 */
class ContactArticleData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'article_id',
        'text',
    ];
}
