<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @package App\Models
 * @method static create(array $array)
 * @method static firstOrCreate(array $array, array $array1)
 * @method static find($id)
 * @method static whereId($id)
 * @method static hotelUsers()
 * @property $hotel_id
 * @property $hotel_admin
 */
class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'hotel_id',
        'role_id',
        'crm_role_id',
        'hotel_main',
        'crm',
        'name',
        'email',
        'phone',
        'password',
		'blocked_at',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'blocked_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function language(): BelongsTo
    {
        return $this->belongsTo('App\Models\Language', 'lang_id', 'id');
    }

    /**
     * Hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function hotel(): BelongsTo
    {
        return $this->belongsTo('App\Models\Hotels\Hotel', 'hotel_id', 'id');
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function crmRole(): BelongsTo
    {
        return $this->belongsTo('App\Models\Permissions\CrmRole', 'crm_role_id', 'id');
    }

    /**
     * Languages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function languages(): HasMany
    {
        return $this->hasMany(
            'App\Models\Hotels\HotelLanguage',
            'hotel_id',
            'hotel_id'
        )
            ->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services(): HasMany
    {
        return $this->hasMany('App\Models\Hotels\HotelService', 'hotel_id', 'hotel_id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelUsers(Builder $query): Builder
    {
        return $query->where(
            [
                'hotel_id' => auth()->user()->hotel_id,
                'crm' => true,
            ]
        );
    }
}
