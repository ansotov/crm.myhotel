<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 *
 * @package App\Models
 * @method static where(bool[] $array)
 */
class Currency extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql_geo';
}
