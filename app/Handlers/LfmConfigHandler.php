<?php

namespace App\Handlers;

use UniSharp\LaravelFilemanager\Handlers\ConfigHandler;

/**
 * Class LfmConfigHandler
 *
 * @package App\Handlers
 */
class LfmConfigHandler extends ConfigHandler
{
    /**
     * @return int|string|null
     */
    public function userField()
    {
        return parent::userField();
    }
}
