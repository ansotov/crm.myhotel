<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

/**
 * Class HotelAdmin
 *
 * @package App\Http\Middleware
 */
class HotelAdmin
{
	/**
	 * @param          $request
	 * @param \Closure $next
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
	 */
	public function handle($request, Closure $next)
	{
		if (
			$request->user()
			&& $request->user()->crm == true
			&& is_null($request->user()->blocked_at)
		) {
			return $next($request);
		} else {
			auth()->logout();

			return redirect(route('login'));
		}
	}

}
