<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Models\Orders\Order;
use App\Models\Orders\OrderStatus;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class OrdersController
 *
 * @package App\Http\Controllers\Hotel
 */
class OrdersController extends Controller
{
	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Contracts\View\View
	 */
	public function index(Request $request): View
	{
		$this->items = Order::filter($request->all())
			->where(function ($query) {
				$query->hotelOrders();
			})
			->paginate(setting('site.perpage'));

		return view(
			'hotel.orders.items',
			[
				'items'    => $this->items,
				'statuses' => OrderStatus::all(),
			]
		);
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		return view(
			'hotel.orders.show',
			[
				'item'     => Order::whereId($id)->first(),
				'statuses' => OrderStatus::all(),
			]
		);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param                          $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id): RedirectResponse
	{
		$request->validate([
			'status_id' => 'required',
		]);

		$item            = Order::find($id);
		$item->status_id = $request['status_id'];

		$item->save();

		return back()->with('status', __('forms.updated'));
	}
}
