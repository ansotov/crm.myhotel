<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Models\Rooms\Room;
use App\Models\Rooms\RoomType;
use Exception;
use Illuminate\Http\Request;

/**
 * Class RoomsController
 *
 * @package App\Http\Controllers\Hotel
 */
class RoomsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->trashed) {
            $this->items = Room::withTrashed()->hotelRooms();
        } elseif ($request->onlyTrashed) {
            $this->items = Room::onlyTrashed()->hotelRooms();
        } else {
            $this->items = Room::hotelRooms();
        }

        return view(
            'hotel.rooms.items',
            [
                'items' => $this->items->paginate(setting('site.perpage'))
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('hotel.rooms.create', ['types' => RoomType::all()]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'items' => 'required|array',
        ]);

        foreach ($request->items as $v) {
            foreach (explode(',', $v['numbers']) as $v2) {
                $number = trim($v2);
                if (empty($number)) {
                    continue;
                }

                $item = new Room([
                    'number'   => $number,
                    'hotel_id' => auth()->user()->hotel_id,
                    'type_id'  => $v['type_id'],
                    'comment'  => $request->get('comment'),
                    'active'   => (bool)$request->get('active'),
                ]);
                try {
                    $item->save();
                } catch (Exception $exception) {
                    if ((int)$exception->getCode() === self::ERROR_DOUBLE) {
                        $this->errors[] = __('errors.room_double', ['number' => $number]);
                    } else {
                        $this->errors[] = $exception->getMessage();
                    }
                }
            }
        }

        return redirect(route('hotel.rooms.index'))->with(
            [
                'status' => __('forms.saved')
            ]
        )->withErrors($this->errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('hotel.rooms.edit', [
            'item'  => Room::find($id),
            'types' => RoomType::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'number'  => 'required|unique:rooms,number,' . $id,
            'type_id' => 'required',
        ]);

        $item          = Room::find($id);
        $item->number  = $request->get('number');
        $item->type_id = $request->get('type_id');
        $item->comment = $request->get('comment');
        $item->active  = (bool)$request->get('active');

        $item->save();

        return redirect(route('hotel.rooms.index'))->with('status', __('forms.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        Room::where(['id' => $id])->delete();

        return redirect(route('hotel.rooms.index', ['onlyTrashed' => true]))->with('status', __('forms.deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore(int $id)
    {
        Room::where(['id' => $id])->restore();

        return redirect(route('hotel.rooms.index', ['trashed' => true]))->with('status', __('forms.restored'));
    }
}
