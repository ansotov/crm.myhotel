<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Jobs\Hotels\SettingsMainDataTranslate;
use App\Models\Currency;
use App\Models\Hotels\Hotel;
use App\Models\Hotels\HotelCurrency;
use App\Models\Hotels\HotelData;
use App\Models\Hotels\HotelLanguage;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;

/**
 * Class SettingsController
 *
 * @package App\Http\Controllers\Hotel
 */
class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->items = Language::where(['active' => true])->get();

        return view(
            'hotel.settings.main',
            [
                'items' => $this->items,
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function languages()
    {
        $items = Language::where(['active' => true]);

        $items->leftJoin('language_data', 'languages.id', '=', 'language_data.language_id');
        $items->where('language_data.lang_id', '=', $this->currentLanguage()->id);
        $items->orderBy('language_data.title');

        $this->items = $items->get();

        $languages = $this->hotelLanguages(auth()->user()->hotel_id);

        return view(
            'hotel.settings.languages',
            [
                'items'     => $this->items,
                'languages' => $languages->pluck('lang_id'),
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function currencies()
    {
        $this->items = Currency::where(['active' => true])->get();

        $currencies = $this->hotelCurrencies(auth()->user()->hotel_id);

        return view(
            'hotel.settings.currencies',
            [
                'items'      => $this->items,
                'currencies' => $currencies->pluck('currency_id'),
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function update(Request $request): string
    {
        // Languages
        $languages = $request->get('languages');

        if ($languages) {
            // Delete old information
            HotelLanguage::where(['hotel_id' => auth()->user()->hotel_id])->delete();

            foreach (array_flip($this->mainLanguages()->pluck('id')->toArray()) as $k => $v) {
                $languages[$k] = $v;
            }

            foreach ($languages as $k => $v) {
                try {
                    HotelLanguage::create(
                        [
                            'hotel_id' => auth()->user()->hotel_id,
                            'lang_id'  => $k,
                        ]
                    );
                } catch (Exception $exception) {
                    return $exception->getMessage();
                }
            }
        }

        // Data
        $settings = $request->get('settings');

        if ($settings) {
            Hotel::whereId(auth()->user()->hotel_id)
                ->update(
                    [
                        'title' => $settings['title'],
                    ]
                );

            $item = Hotel::find(auth()->user()->hotel_id);

            // Delete old information
            HotelData::where(['hotel_id' => auth()->user()->hotel_id])->delete();

            // Queue translations
            SettingsMainDataTranslate::dispatch($item, $settings, auth()->user()->language, auth()->user());
        }

        // Currencies
        $currencies = $request->get('currencies');

        if ($currencies) {
            foreach (array_flip($this->mainCurrencies()->pluck('id')->toArray()) as $k => $v) {
                $currencies[$k] = $v;
            }

            // Delete old information
            HotelCurrency::where(['hotel_id' => auth()->user()->hotel_id])->delete();

            foreach ($currencies as $k => $v) {
                try {
                    HotelCurrency::create(
                        [
                            'hotel_id'    => auth()->user()->hotel_id,
                            'currency_id' => $k,
                        ]
                    );
                } catch (Exception $exception) {
                    return $exception->getMessage();
                }
            }
        }

        return back()->with('status', __('forms.edited'));
    }
}
