<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Interfaces\RoleCodes;
use App\Models\Permissions\CrmRole;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\User
 */
class UsersController extends Controller implements RoleCodes
{
	/**
	 * @var \App\Models\User
	 */
	private $user;

	/**
	 * UsersController constructor.
	 *
	 * @param \App\Models\User $user
	 */
	public function __construct(
		User $user
	) {
		$this->user = $user;
	}

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$this->items = User::hotelUsers()
			->paginate(setting('site.perpage'));

		return view(
			'hotel.users.items',
			[
				'items' => $this->items
			]
		);
	}

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return view(
			'hotel.users.ajax.addCrmUser',
			[
				'roles' => CrmRole::all(),
			]
		);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return void
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name'     => 'required',
			'phone'    => 'required',
			'email'    => 'required|unique:users',
			'lang_id'  => 'required',
			'role_id'  => 'required',
			'password' => 'required|min:8|confirmed',
		]);

		$request->name  = trim($request->name);
		$request->phone = str_replace(' ', '', $request->phone);
		$request->email = trim($request->email);

		User::create(
			[
				'lang_id'  => $request->lang_id,
				'role_id'  => $this->role('user')->id,
				'crm_role_id'  => $request->role_id,
				'hotel_id' => auth()->user()->hotel_id,
				'name'     => $request->name,
				'phone'    => $request->phone,
				'email'    => $request->email,
				'crm'      => true,
				'password' => Hash::make($request->password),
			]
		);
	}

	/**
	 * @param int $userId
	 *
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	public function blocked(
		int $userId
	): RedirectResponse {
		$user        = $this->user->whereId($userId)->first();

		if ($user && $user->id !== auth()->user()->id) {
			$blockedTime = is_null($user->blocked_at) ? now() : null;

			$user->update(
				[
					'blocked_at' => $blockedTime,
				]
			);
		}

		return back();
	}
}
