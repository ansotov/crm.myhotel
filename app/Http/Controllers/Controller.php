<?php

namespace App\Http\Controllers;

use App\Interfaces\ErrorCodes;
use App\Models\Currency;
use App\Models\Language;
use App\Models\Permissions\CrmRole;
use App\Traits\Currencies;
use App\Traits\Languages;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use TCG\Voyager\Models\Role;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 */
class Controller extends BaseController implements ErrorCodes
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Languages, Currencies;

	/**
	 * @var object|array
	 */
	protected $items;

	/**
	 * @var object
	 */
	protected $item;

	/**
	 * @var array
	 */
	protected $errors;

	/**
	 * @var int[]
	 */
	protected $imagesSizes = [
		's_' => 150,
		'm_' => 500,
		'l_' => 900,
	];

	/**
	 * @var int
	 */
	protected $imageQuality = 80;

	/**
	 * @return mixed
	 */
	public function mainLanguages()
	{
		return Language::where(['active' => true, 'required' => true])->get();
	}

	/**
	 * @return mixed
	 */
	public function mainCurrencies()
	{
		return Currency::where(['active' => true, 'required' => true])->get();
	}

	/**
	 * @return mixed
	 */
	protected function currentLanguage()
	{
		return Language::where(['const' => app()->getLocale()])->first();
	}

	/**
	 * @param $const
	 *
	 * @return mixed
	 */
	protected function role($const)
	{
		return Role::where(['name' => $const])->first();
	}

	/**
	 * @param $const
	 *
	 * @return \App\Models\Permissions\CrmRole
	 */
	protected function crmRole($const): CrmRole
	{
		return CrmRole::where(['const' => $const])->first();
	}
}
