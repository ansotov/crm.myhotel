<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 27.04.2021
 * Time: 5:37 PM
 */

namespace App\Interfaces;

/**
 * Interface ErrorCodes
 *
 * @package App\Interfaces
 */
interface ErrorCodes
{
    /**
     * @var int
     */
    const ERROR_DOUBLE = 23000;
}
