<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 27.04.2021
 * Time: 5:37 PM
 */

namespace App\Interfaces;

/**
 * Interface RoleCodes
 *
 * @package App\Interfaces
 */
interface RoleCodes
{
    const CRM_ROLES = [
        'hotel_admin',
        'hotel_admin',
        'reception'
    ];
}
