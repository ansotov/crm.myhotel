<?php 
return [
  'apply' => 'Applicare',
  'cancel' => 'Annulla',
  'from' => 'A partire dal',
  'to' => 'Per',
  'custom' => 'Abitudine',
];