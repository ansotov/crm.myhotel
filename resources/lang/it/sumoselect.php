<?php 
return [
  'select-here' => 'Seleziona qui',
  'selected' => 'Selezionato',
  'all-selected' => 'Tutti selezionati!',
  'search' => 'Ricerca...',
  'no-matches' => 'Nessuna corrispondenza per',
  'ok' => 'Ok',
  'cancel' => 'Annulla',
  'select-all' => 'Seleziona tutto',
];