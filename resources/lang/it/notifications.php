<?php 
return [
  'main-language' => 'La lingua di base è :lang, se manterrai alcuni campi vuoti, si tradurrà con il traduttore di google, ma senza formattare il testo. per una migliore visualizzazione è necessario formattare manualmente il testo.',
];