<?php 
return [
  'main' => 'Principale',
  'profile' => 'Profil',
  'orders' => 'Ordres',
  'rooms' => 'Pièces',
  'guests' => 'Invités',
  'services' => 'Prestations de service',
  'offers' => 'Des offres',
  'news' => 'Nouvelles',
  'articles' => 'Des articles',
  'logout' => 'Se déconnecter',
  'hotels' => 'Hôtels',
  'login' => 'S\'identifier',
  'settings' => 'Paramètres',
  'settings.hotel' => 'Paramètres de l\'hôtel',
];