<?php 
return [
  'select-here' => 'Sélectionnez ici',
  'selected' => 'Choisi',
  'all-selected' => 'Tous sélectionnés!',
  'search' => 'Chercher...',
  'no-matches' => 'Aucun résultat pour',
  'ok' => 'D\'accord',
  'cancel' => 'Annuler',
  'select-all' => 'Tout sélectionner',
];