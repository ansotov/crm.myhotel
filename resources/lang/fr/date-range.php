<?php 
return [
  'apply' => 'Appliquer',
  'cancel' => 'Annuler',
  'from' => 'De',
  'to' => 'À',
  'custom' => 'Personnalisé',
];