<?php 
return [
  'rooms' => 'Pièces',
  'add' => 'Ajouter',
  'edit' => 'Éditer',
  'hotel' => 'Hôtel',
  'guests' => 'Invités',
  'services' => 'Prestations de service',
  'offers' => 'Des offres',
  'news' => 'Nouvelles',
  'articles' => 'Des articles',
  'profile' => 'Profil',
  'order' => 'Ordre',
  'orders' => 'Ordres',
  'contacts' => 'Contacts',
  'crm-users' => 'Utilisateurs de la CRM',
];