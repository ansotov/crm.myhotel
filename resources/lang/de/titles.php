<?php 
return [
  'rooms' => 'Räume',
  'add' => 'hinzufügen',
  'edit' => 'bearbeiten',
  'hotel' => 'Hotel',
  'guests' => 'Gäste',
  'services' => 'Dienstleistungen',
  'offers' => 'Bietet an',
  'news' => 'Nachrichten',
  'articles' => 'Artikel',
  'profile' => 'Profil',
  'order' => 'Auftrag',
  'orders' => 'Aufträge',
  'contacts' => 'Kontakte',
  'crm-users' => 'CRM-Benutzer',
];