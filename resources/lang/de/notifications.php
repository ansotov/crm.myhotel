<?php 
return [
  'main-language' => 'Die Grundsprache ist :lang. Wenn Sie einige Felder leer lassen, wird sie mit Google Übersetzer übersetzt, jedoch ohne Text zu formatieren. Zur besseren Ansicht sollten Sie Text manuell formatieren.',
];