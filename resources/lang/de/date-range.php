<?php 
return [
  'apply' => 'Anwenden',
  'cancel' => 'Stornieren',
  'from' => 'Von',
  'to' => 'Zu',
  'custom' => 'Benutzerdefiniert',
];