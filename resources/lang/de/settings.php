<?php 
return [
  'language' => 'Sprache',
  'languages' => 'Sprachen',
  'languages.description' => 'Wählen sie sprachen für ihr system',
  'main_information' => 'Hauptinformation',
  'main_information.description' => 'Beschreibung ihres hotels',
  'main' => 'Main',
  'currencies' => 'Währungen',
  'currencies.description' => 'Wählen sie währungen für ihr system',
  'contacts' => 'Kontakte',
  'contacts.description' => 'Kontakte Ihres Hotels',
  'crm-users' => 'CRM-Benutzer',
  'role' => 'Rolle',
];