<?php 
return [
  'rooms' => 'Pokoje',
  'add' => 'přidat',
  'edit' => 'upravit',
  'hotel' => 'Hotel',
  'guests' => 'Hosté',
  'services' => 'Služby',
  'offers' => 'Nabídky',
  'news' => 'Zprávy',
  'articles' => 'Články',
  'profile' => 'Profil',
  'order' => 'Objednat',
  'orders' => 'Objednávky',
  'contacts' => 'Kontakty',
  'crm-users' => 'Uživatelé CRM',
];