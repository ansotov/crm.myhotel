<?php 
return [
  'add' => 'Přidat',
  'restore' => 'Obnovit',
  'edit' => 'Upravit',
  'delete' => 'Odstranit',
  'reset' => 'Resetovat',
  'save' => 'Uložit',
  'search' => 'Vyhledávání',
  'search.placeholder' => 'Hledat...',
  'yes' => 'Ano',
  'no' => 'Ne',
  'send' => 'Poslat',
  'in-archive' => 'V archivu',
  'new-user' => 'Nový uživatel',
  'blocked' => 'Blokováno',
  'block' => 'Blok',
  'unblock' => 'Odblokovat',
];