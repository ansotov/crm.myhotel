<?php

return [
	'apply'     => 'Apply',
	'cancel'    => 'Cancel',
	'from'      => 'From',
	'to'        => 'To',
	'custom'    => 'Custom',
];
