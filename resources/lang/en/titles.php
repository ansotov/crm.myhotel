<?php

return array(
    'rooms'     => 'Rooms',
    'add'       => 'add',
    'edit'      => 'edit',
    'hotel'     => 'Hotel',
    'guests'    => 'Guests',
    'services'  => 'Services',
    'offers'    => 'Offers',
    'news'      => 'News',
    'articles'  => 'Articles',
    'profile'   => 'Profile',
    'order'     => 'Order',
    'orders'    => 'Orders',
    'contacts'  => 'Contacts',
    'crm-users' => 'Crm users',
);
