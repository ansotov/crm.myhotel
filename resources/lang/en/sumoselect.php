<?php

return [
    'select-here'  => 'Select Here',
    'selected'     => 'Selected',
    'all-selected' => 'all selected!',
    'search'       => 'Search...',
    'no-matches'   => 'No matches for',
    'ok'           => 'Ok',
    'cancel'       => 'Cancel',
    'select-all'   => 'Select All',
];
