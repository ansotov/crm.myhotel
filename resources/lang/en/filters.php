<?php

return [
	'status' => 'Status',
	'search' => 'Search',
	'date-range' => 'Date from and to'
];
