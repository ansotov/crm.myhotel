@extends('layouts.auth')

@section('content')
    <div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('password.update') }}" class="ajax-send">
                        <h1>{{ __('forms.reset-password') }}</h1>
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input
                                    placeholder="{{ __('forms.email') }}"
                                    id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email"
                                    value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input
                                    placeholder="{{ __('forms.password') }}"
                                    id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror"
                                    name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input
                                    placeholder="{{ __('forms.confirm-password') }}"
                                    id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required
                                    autocomplete="new-password">
                            </div>
                        </div>

                        <div class="col-md-12 mb-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-b">
                                    {{ __('forms.reset-password') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">
                            <a href="{{ route('login') }}" class="to_register">{{ __('forms.login') }}</a> |
                            <a href="{{ route('register') }}" class="to_register">{{ __('forms.registration') }}</a>
                        </p>

                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <p>©2019-{{ date('Y') }} {{ __('main.full-copyright') }}</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection()
