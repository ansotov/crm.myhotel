@extends('layouts.auth')

@section('content')
    <div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('register') }}" class="form-a ajax-send">
                        <h1>{{ __('forms.registration') }}</h1>
                        @csrf
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="name">{{ __('forms.hotel_name') }}</label>
                                    <input
                                        id="name" type="text"
                                        class="form-control form-control-lg form-control-a @error('hotel_name') is-invalid @enderror"
                                        name="hotel_name"
                                        value="{{ old('hotel_name') }}" required autocomplete="name" autofocus>

                                    @error('hotel_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="name">{{ __('forms.name') }}</label>
                                    <input
                                        id="name" type="text"
                                        class="form-control form-control-lg form-control-a @error('name') is-invalid @enderror"
                                        name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="email">{{ __('forms.email') }}</label>
                                    <input
                                        id="email" type="email"
                                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                                        name="email"
                                        value="{{ old('email') }}" required autocomplete="name" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="lang_id">{{ __('settings.language') }}</label>
                                    <select
                                        class="form-control form-control-lg form-control-a" name="lang_id"
                                        id="lang_id">
                                        @foreach(resolve(\App\Http\Controllers\Controller::class)->mainLanguages() as $k => $v)
                                            <option
                                                value="{{ $v->id }}"{{ app()->getLocale() == $v->const ? ' selected' : false }}>{{ $v->data->title }}</option>
                                        @endforeach
                                    </select>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="password">{{ __('forms.password') }}</label>
                                    <input
                                        id="password" type="password"
                                        class="form-control form-control-lg form-control-a @error('password') is-invalid @enderror"
                                        name="password"
                                        value="{{ old('password') }}" required>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="password_confirmation">{{ __('forms.password_confirmation') }}</label>
                                    <input
                                        id="password_confirmation" type="password"
                                        class="form-control form-control-lg form-control-a @error('password_confirmation') is-invalid @enderror"
                                        name="password_confirmation"
                                        value="{{ old('password_confirmation') }}" required>

                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-b">
                                        {{ __('forms.registration') }}
                                    </button>

                                    <a
                                        class="btn btn-link"
                                        href="{{ route('login') }}">
                                        {{ __('forms.login') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">{{ __('forms.is-user') }}
                            <a href="{{ route('login') }}" class="to_register">{{ __('forms.login') }}</a>
                        </p>

                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <p>©2019-{{ date('Y') }} {{ __('main.full-copyright') }}</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection()
