@extends('layouts.app')

@section('title')
    {{ __('titles.articles') }}: {{ __('titles.add') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.articles.store')]) !!}

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    @foreach(auth()->user()->languages as $k => $v)
                        <li class="nav-item">
                            <a
                                href="#{{ $v->language->const }}" class="nav-link{{ $v->language->const == auth()->user()->language->const ? ' active' : false }}"
                                id="{{ $v->language->const }}-tab" data-toggle="tab"
                                role="tab"
                                aria-controls="{{ $v->language->const }}"
                                aria-selected="{{ $k == 0 ? 'true' : 'false' }}">{{ $v->language->data->title }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="alert alert-info" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span> {{ __('notifications.main-language', ['lang' => auth()->user()->language->title]) }}
                    </div>
                    @foreach(auth()->user()->languages as $k => $v)
                        <div
                            class="tab-pane fade{{ $v->language->const == auth()->user()->language->const ? ' show active' : false }}"
                            id="{{ $v->language->const }}"
                            role="tabpanel"
                            aria-labelledby="{{ $v->language->const }}-tab">
                            <div class="form-group col-md-4">
                                {!! Form::label('title', __('tables.title')) !!}
                                {!! Form::text('title[' . $v->language->id . ']', old('title[' . $v->language->id . ']'), ['class' => 'form-control', 'required' => ($v->language->id == auth()->user()->lang_id ? true : false)]) !!}
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-12">
                                {!! Form::label('description', __('tables.description')) !!}
                                {!! Form::textarea('description[' . $v->language->id . ']', old('description[' . $v->language->id . ']'), ['class' => 'form-control ckeditor']) !!}
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-12">
                                {!! Form::label('text', __('tables.text')) !!}
                                {!! Form::textarea('text[' . $v->language->id . ']', old('text[' . $v->language->id . ']'), ['class' => 'form-control ckeditor']) !!}
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="form-group col-md-2">
                    {!! Form::label('published_at', __('tables.published_at')) !!}
                    {!! Form::date('published_at', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('active', __('tables.active')) !!}
                    {!! Form::checkbox('active', null) !!}
                </div>

                <div class="form-group col-md-12">
                    {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
