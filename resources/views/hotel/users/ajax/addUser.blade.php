<h3>{{ __('tables.user') }}: {{ __('titles.add') }}</h3>

<ul class="formErrors"></ul>

<form method="POST" action="{{ route('hotel.users.store') }}" class="form-a ajax-send">
    @csrf
    <div class="row">
        <div class="form-group col-md-12">
            {!! Form::label('name', __('forms.name')) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('phone', __('forms.phone')) !!}
            {!! Form::text('phone', null, ['class' => 'form-control', 'required' => true]) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('email', __('forms.email')) !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            <label for="lang_id">{{ __('settings.language') }}</label>
            <select
                class="form-control"
                name="lang_id"
                required
                id="lang_id">
                @foreach(resolve(\App\Http\Controllers\Controller::class)->mainLanguages() as $k => $v)
                    <option
                        value="{{ $v->id }}"{{ app()->getLocale() == $v->const ? ' selected' : false }}>{{ $v->data->title }}</option>
                @endforeach
            </select>

            @error('lang_id')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('personalData', __('forms.personal-data')) !!}
            {!! Form::textarea('personalData', null, ['class' => 'form-control']) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            {{ Form::submit(__('buttons.add'), ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</form>
