<div class="filters">
	<div class="row">
		<div class="form-row" style="width: 100%">
			<form class="col-md-8">
				<div class="form-group">
					<div class="col-lg-7">
						<label>{{ __('filters.search') }}</label>
						<input type="text" placeholder="Search..." class="form-control" id="search" name="search" value="{{ request()->search }}">
					</div>
					<div class="col-lg-4">
						<label>{{ __('filters.date-range') }}</label>
						<input type="text" class="form-control dateRange" name="dateRange" value="{{ request()->dateRange }}">
					</div>
					<div class="col-lg-1 col-md-3 col-sm-12 pt-4 p-0">
						<button type="submit" class="btn btn-base">
							<svg
								xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
								fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round" class="feather feather-search">
								<circle cx="11" cy="11" r="8"></circle>
								<line x1="21" y1="21" x2="16.65" y2="16.65"></line>
							</svg>
						</button>
					</div>
				</div>
			</form>
			<div class="col-md-4 d-flex justify-content-end">
				<div class="form-group">
					<label>{{ __('filters.status') }}</label>
					<select
						class="form-control sumoselect SumoUnder" name="status_id"
						onchange="if (this.value) window.location.href=this.value">
						<option value="{{ route('hotel.orders.index') }}">------</option>
						@foreach($statuses as $k => $v)
							<option
								value="?status_id={{ $v->id }}"{{ isset(request()->filters) && request()->filters['status_id'] == $v->id ? ' selected' : null }}>{{ $v->data->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
