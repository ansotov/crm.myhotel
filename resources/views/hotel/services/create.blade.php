@extends('layouts.app')

@section('title')
    {{ __('titles.services') }}: {{ __('titles.add') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.services.store'), 'id' => 'uploadForm', 'enctype' => 'multipart/form-data']) !!}

                <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                        @foreach(auth()->user()->languages as $k => $v)
                            <li class="nav-item">
                                <a
                                    href="#{{ $v->language->const }}" class="nav-link{{ $v->language->const == auth()->user()->language->const ? ' active' : false }}"
                                    id="{{ $v->language->const }}-tab" data-toggle="tab"
                                    role="tab"
                                    aria-controls="{{ $v->language->const }}"
                                    aria-selected="{{ $k == 0 ? 'true' : 'false' }}">{{ $v->language->data->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content" id="myTabContent">

                        <div class="alert alert-info" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span> {{ __('notifications.main-language', ['lang' => auth()->user()->language->title]) }}
                        </div>
                        @foreach(auth()->user()->languages as $k => $v)
                            <div
                                class="tab-pane fade{{ $v->language->const == auth()->user()->language->const ? ' show active' : false }}"
                                id="{{ $v->language->const }}"
                                role="tabpanel"
                                aria-labelledby="{{ $v->language->const }}-tab">
                                <div class="form-group col-md-4">
                                    {!! Form::label('title', __('tables.title')) !!}
                                    {!! Form::text('title[' . $v->language->id . ']', old('title[' . $v->language->id . ']'), ['class' => 'form-control', 'required' => ($v->language->id == auth()->user()->lang_id ? true : false)]) !!}
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group col-md-12">
                                    {!! Form::label('description', __('tables.description')) !!}
                                    {!! Form::textarea('description[' . $v->language->id . ']', old('description[' . $v->language->id . ']'), ['class' => 'form-control ckeditor']) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-12">
                    {!! Form::label('price', __('tables.images')) !!}

                    <label for="profile_image"></label>

                    <div class="attachment">
                        <input type="file" id="images" accept="image/*"  name="images[]" multiple/>
                        <ul class="newImages"></ul>
                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-3">
                    {!! Form::label('price', __('tables.price')) !!}
                    {!! Form::number('price', false, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-md-1">
                    {!! Form::label('currency_id', __('tables.currency')) !!}
                    <select name="currency_id" class="form-control sumoselect">
                        <option value="">---</option>
                        @foreach($currencies as $k => $v)
                            <option
                                {{ old('currency_id') == $v->id ? 'selected' : false }}
                                value="{{ $v->id }}">{{ $v->code }} - {{ $v->symbol }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-3">
                    {!! Form::label('type_id', __('tables.category')) !!}
                    <select name="type_id" class="form-control sumoselect">
                        <option value="">---</option>
                        @foreach($types as $k => $v)
                            <option
                                {{ old('type_id') == $v->id ? 'selected' : false }}
                                value="{{ $v->id }}">{{ $v->data->title }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-12">
                    {!! Form::label('active', __('tables.active')) !!}
                    {!! Form::checkbox('active', null, true) !!}
                </div>

                <div class="form-group col-md-12">
                    {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script>

        $('#uploadForm').on('change', function(e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: "{{ route('hotel.services.imageUploader') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (data) => {

                    $('input#images').val('');

                    $.each( data.images, function( key, value ) {
                        $('.attachment ul.newImages').append("<li>" +
                            "<a class='atch-thumb' href='javascript:void(0);'><img src='" + value + "'></a>" +
                            "</li>");
                    });
                },
                error: function(data){

                    $.each( data.responseJSON.errors, function( key, value ) {
                        $('.attachment ul.newImages').append("<li>" +
                            "<a class='atch-thumb' href='javascript:void(0);'><img src='" + value + "'></a>" +
                            "</li>");
                    });
                }
            });
        });

    </script>
@endsection
