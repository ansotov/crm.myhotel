@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }

@endphp

@foreach ($items as $item)

    @php

        $originalItem = $item;

        if (!isset(\App\Models\Permissions\CrmPermission::hasAccess($item->route)->id)) {
            continue;
        }

        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }

        $isActive = null;
        $styles = null;
        $icon = null;

        // Background Color or Color
        if (isset($options->color) && $options->color == true) {
            $styles = 'color:'.$item->color;
        }
        if (isset($options->background) && $options->background == true) {
            $styles = 'background-color:'.$item->color;
        }

        // Check if link is current
        if(url($item->link()) == url()->current()){
            $isActive = 'active ';
        }

        // Set Icon
        if(isset($item->icon_class)){
            $icon = '<i class="fa ' . $item->icon_class . '"></i>';
        }

    @endphp

    <li class="{{ $isActive }}{{ isset($options->class) ? ' ' . $options->class : false }}">
        <a href="{{ $item->link() && $originalItem->children->isEmpty() ? url($item->link()) : 'javascript:void(0);' }}"{{ isset($options->li_a_class) ? ' class=' . $options->li_a_class : false }} target="{{ $item->target }}">
            {!! $icon !!}
            <span>{{ __($item->title) }}</span>
            @if(!$originalItem->children->isEmpty())
                <span class="fa fa-chevron-down"></span>
            @endif
        </a>
        @if(!$originalItem->children->isEmpty())
            <ul class="nav child_menu">
                @include('vendor/voyager/menu/main-left', ['items' => $originalItem->children, 'options' => $options])
            </ul>
        @endif
    </li>
@endforeach
