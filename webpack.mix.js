const mix = require('laravel-mix');
let minifier = require('minifier');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/')
    .js('resources/js/front/app.js', 'public/front/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/front/app.scss', 'public/front/css')
    .then(() => {
        minifier.minify('public/css/app.css'),
        minifier.minify('public/js/app.js'),
        minifier.minify('public/front/css/app.css'),
        minifier.minify('public/front/js/app.js')
    })
    .options({
        postCss: [
            require('postcss-discard-comments')({
                removeAll: true
            })
        ],
        uglify: {
            uglifyOptions: {
                comments: false
            },
        }
    });
