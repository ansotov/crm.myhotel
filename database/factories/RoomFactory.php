<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rooms\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'hotel_id' => 2,
        'type_id'  => $faker->numberBetween(1, 3),
        'number'   => $faker->unique()->numberBetween(1, 100),
        'comment'  => $faker->text(),
        'active'   => 1
    ];
});
