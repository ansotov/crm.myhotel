<?php

namespace Tests\Unit\Users;

use App\Models\Hotels\Guest;
use App\Models\Services\Service;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create();
        //$posts = factory(Guest::class, 5)->create(['user_id' => $user->id]);

        $this->assertNotEmpty($user->id);
        //$this->assertNotEmpty($posts);

        //$this->assertEquals(5, $posts->fresh()->count());
        $this->assertEquals($user->id, 3);
    }
}
