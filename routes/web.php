<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// Requests
Route::group(
    ['middleware' => 'hotel.admin'],
    function () {

        // Main
        Route::get('/', 'MainController@index')->name('main');

        // Orders
        Route::resources(['orders' => 'Hotel\OrdersController'], ['as' => 'hotel']);
        Route::get('/orders/restore/{id}', 'Hotel\OrdersController@restore')->name('hotel.orders.restore');

        // Rooms
        Route::resources(['rooms' => 'Hotel\RoomsController'], ['as' => 'hotel']);
        Route::get('/rooms/restore/{id}', 'Hotel\RoomsController@restore')->name('hotel.rooms.restore');

        // Users
        Route::resources(['users' => 'Hotel\UsersController'], ['as' => 'hotel']);
        Route::get('/search-user', 'Hotel\UsersController@searchUser')->name('hotel.users.searchUser');

        // Guests
        Route::resources(['guests' => 'Hotel\GuestsController'], ['as' => 'hotel']);
        Route::get('/guests/restore/{id}', 'Hotel\GuestsController@restore')->name('hotel.guests.restore');

        // Services
        Route::resources(['services' => 'Hotel\ServicesController'], ['as' => 'hotel']);
        Route::post('/services/update/{id}', 'Hotel\ServicesController@update')->name('hotel.services.update');
        Route::get('/services/restore/{id}', 'Hotel\ServicesController@restore')->name('hotel.services.restore');
        Route::post('/services/image-uploader', 'Hotel\ServicesController@imageUploader')
            ->name('hotel.services.imageUploader');
        Route::post('/services/image-delete', 'Hotel\ServicesController@destroyImage')
            ->name('hotel.services.destroyImage');
        Route::get('/services/archive/{id}/{value}', 'Hotel\ServicesController@archive')
            ->name('hotel.services.archive');

        // Offers
        Route::resources(['offers' => 'Hotel\OffersController'], ['as' => 'hotel']);
        Route::post('/offers/update/{id}', 'Hotel\OffersController@update')->name('hotel.offers.update');
        Route::get('/offers/archive/{id}/{value}', 'Hotel\OffersController@archive')->name('hotel.offers.archive');
        Route::post('/offers/uploader/image-uploader', 'Hotel\OffersController@imageUploader')
            ->name('hotel.offers.imageUploader');
        Route::post('/offers/uploader/image-delete', 'Hotel\OffersController@destroyImage')
            ->name('hotel.offers.destroyImage');

        // News
        Route::resources(['news' => 'Hotel\NewsController'], ['as' => 'hotel']);
        Route::get('/news/archive/{id}/{value}', 'Hotel\NewsController@archive')->name('hotel.news.archive');

        // Articles
        Route::resources(['articles' => 'Hotel\ArticlesController'], ['as' => 'hotel']);
        Route::get('/articles/archive/{id}/{value}', 'Hotel\ArticlesController@archive')
            ->name('hotel.articles.archive');
        Route::get('/hotel/contacts', 'Hotel\ArticlesController@contacts')->name('hotel.contacts');
        Route::patch('/hotel/contacts', 'Hotel\ArticlesController@contactsUpdate')->name('hotel.contacts.update');

        // Settings
        Route::group(['prefix' => 'settings'], function () {
            Route::get('/', 'Hotel\SettingsController@index')->name('hotel.settings.index');
            Route::put('/', 'Hotel\SettingsController@update')->name('hotel.settings.update');
            Route::get('/languages', 'Hotel\SettingsController@languages')->name('hotel.settings.languages');
            Route::get('/currencies', 'Hotel\SettingsController@currencies')->name('hotel.settings.currencies');
        });

        Route::get('/translation', 'MainController@translation')->name('translation');

        // Profile
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/settings', 'User\Profile\SettingsController@index')->name('profile.settings');
            Route::patch('/settings', 'User\Profile\SettingsController@update')->name('profile.settings.update');
        });

        // Users
        Route::resources(['crm-users' => 'User\UsersController'], ['as' => 'hotel']);
		Route::get('/crm-users/block/{id}', 'User\UsersController@blocked')
			->name('hotel.users.block');
    }
);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
